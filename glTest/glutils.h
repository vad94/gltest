#ifndef GLUTILS_H
#define GLUTILS_H

#pragma once

#include <stdio.h>
#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <sys/time.h>

#include <string>

class MessageException
{
public:
    MessageException(std::string message): m_message(message) {};
    virtual ~MessageException() {};
    std::string GetMessage() const {return m_message;};
protected:
    std::string m_message;
};

static void print_extension_list(const char *ext)
{
   const char indentString[] = "    ";
   const int indent = 4;
   const int max = 79;
   int width, i, j;

   if (!ext || !ext[0])
      return;

   width = indent;
   printf("%s", indentString);
   i = j = 0;
   while (1) {
      if (ext[j] == ' ' || ext[j] == 0) {
         /* found end of an extension name */
         const int len = j - i;
         if (width + len > max) {
            /* start a new line */
            printf("\n");
            width = indent;
            printf("%s", indentString);
         }
         /* print the extension name between ext[i] and ext[j] */
         while (i < j) {
            printf("%c", ext[i]);
            i++;
         }
         /* either we're all done, or we'll continue with next extension */
         width += len + 1;
         if (ext[j] == 0) {
            break;
         }
         else {
            i++;
            j++;
            if (ext[j] == 0)
               break;
            printf(", ");
            width += 2;
         }
      }
      j++;
   }
   printf("\n");
}

static void egl_print_info(EGLDisplay egl_dpy)
{
    const char *s;

    s = eglQueryString(egl_dpy, EGL_VERSION);
    printf("EGL_VERSION: %s\n", s);

    s = eglQueryString(egl_dpy, EGL_VENDOR);
    printf("EGL_VENDOR: %s\n", s);

    s = eglQueryString(egl_dpy, EGL_EXTENSIONS);
    printf("EGL_EXTENSIONS:\n", s);
    print_extension_list((char *) s);

    s = eglQueryString(egl_dpy, EGL_CLIENT_APIS);
    printf("EGL_CLIENT_APIS: %s\n", s);
}

static void gl_print_info()
{
   printf("GL_VERSION: %s\n", (char *) glGetString(GL_VERSION));
   printf("GL_RENDERER: %s\n", (char *) glGetString(GL_RENDERER));
   printf("GL_EXTENSIONS:\n");
   print_extension_list((char *) glGetString(GL_EXTENSIONS));
}


static unsigned long GetTickCount()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (tv.tv_sec*1000+tv.tv_usec/1000);
}

#endif // GLUTILS_H
