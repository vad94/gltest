#include "cube_renderer.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

//math
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat4x4.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/constants.hpp"

#include "glutils.h"

const char* pVSFileName = "/shaders/shader.vs";
const char* pFSFileName = "/shaders/shader.fs";

bool CubeRenderer::Init(boost::weak_ptr<const GLRenderEngine> engine)
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glClearColor(1.f,0.f,1.f,1.f);
    glClear(GL_COLOR_BUFFER_BIT);

    GLubyte *data = (GLubyte*) malloc(4);
    glReadPixels(0,0,1,1,GL_RGBA, GL_UNSIGNED_BYTE, data);

    std::cout << std::hex
              << (unsigned int) data[0]
              << (unsigned int) data[1]
              << (unsigned int) data[2]
              << (unsigned int) data[3]
              << std::endl;

    CreateVertexBuffer();
    CompileShaders();
}
void CubeRenderer::Draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    static float Scale = 0.0f;

    Scale += 0.001f;

    glm::mat4x4 proj = glm::perspective(45.f, 320.f/240.f, 0.1f, 100.f);

    glm::mat4x4 view = glm::lookAt(glm::vec3(0,0,5), glm::vec3(0,0,0), glm::vec3(0,1,0) );


    static unsigned long lt = GetTickCount();
    unsigned long dt = GetTickCount() - lt;
    lt = GetTickCount();

    static float angle = 0;
    angle += (3.14f / 1200) * dt;

    glm::mat4x4 model = glm::rotate( glm::mat4(1.0f), angle, glm::vec3(0.3f,0.7f,0.1f) );

    glm::mat4x4 mvp = proj * view * model;

    glm::mat4 t = glm::transpose(mvp);

    glUniformMatrix4fv(gWorldLocation, 1, GL_TRUE, &t[0][0]);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, CBO);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDisableVertexAttribArray(0);
}

void CubeRenderer::Destroy()
{

}


void CubeRenderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
    GLuint ShaderObj = glCreateShader(ShaderType);

    if (ShaderObj == 0) {
        fprintf(stderr, "Error creating shader type %d\n", ShaderType);
        exit(1);
    }

    const GLchar* p[1];
    p[0] = pShaderText;
    int Lengths[1];
    Lengths[0]= strlen(pShaderText);
    glShaderSource(ShaderObj, 1, p, Lengths);
    glCompileShader(ShaderObj);
    GLint success;
    glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLchar InfoLog[1024];
        glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
        fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
        fprintf(stderr, pShaderText);
        exit(1);
    }

    glAttachShader(ShaderProgram, ShaderObj);
}

bool CubeRenderer::ReadFile(const char* pFileName, string& outFile)
{
    char exe_path[128];
    memset(exe_path, 0, 128);
    readlink("/proc/self/exe", exe_path, 128);
    std::string path = std::string(exe_path);
    std::string full = path.substr(0, path.find_last_of("\\/")) + std::string(pFileName);

    ifstream f( full );

    bool ret = false;

    if (f.is_open()) {
        string line;
        while (getline(f, line)) {
            outFile.append(line);
            outFile.append("\n");
        }

        f.close();

        ret = true;
    }
    else
    {
        std::cout << "Error: cannot open file " << full << ": " << strerror(errno) << std::endl;
    }

    return ret;
}

void CubeRenderer::CompileShaders()
{
    GLuint ShaderProgram = glCreateProgram();

    if (ShaderProgram == 0) {
        fprintf(stderr, "Error creating shader program\n");
        exit(1);
    }
    string vs, fs;

    if (!ReadFile(pVSFileName, vs)) {
        exit(1);
    };

    if (!ReadFile(pFSFileName, fs)) {
        exit(1);
    };

    AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);
    AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

    GLint suc = 0;
    GLchar ErrorLog[1024] = { 0 };

    glLinkProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &suc);
    if (suc == 0) {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    glValidateProgram(ShaderProgram);
    glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &suc);
    if (!suc) {
        glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
        fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
        exit(1);
    }

    glUseProgram(ShaderProgram);

    gWorldLocation = glGetUniformLocation(ShaderProgram, "gWorld");
    assert(gWorldLocation != 0xFFFFFFFF);
}

void CubeRenderer::CreateVertexBuffer()
{
    glm::vec3 v[36];
    int i = 0;
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f,-1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3( -1.0f, 1.0f, 1.0f );
    v[i++] = glm::vec3(  1.0f,-1.0f, 1.0f );

    glm::vec3 c[36];
    i = 0;

    c[i++] = glm::vec3( 0.583f,  0.771f,  0.014f );
    c[i++] = glm::vec3( 0.609f,  0.115f,  0.436f );
    c[i++] = glm::vec3( 0.327f,  0.483f,  0.844f );
    c[i++] = glm::vec3( 0.822f,  0.569f,  0.201f );
    c[i++] = glm::vec3( 0.435f,  0.602f,  0.223f );
    c[i++] = glm::vec3( 0.310f,  0.747f,  0.185f );
    c[i++] = glm::vec3( 0.597f,  0.770f,  0.761f );
    c[i++] = glm::vec3( 0.559f,  0.436f,  0.730f );
    c[i++] = glm::vec3( 0.359f,  0.583f,  0.152f );
    c[i++] = glm::vec3( 0.483f,  0.596f,  0.789f );
    c[i++] = glm::vec3( 0.559f,  0.861f,  0.639f );
    c[i++] = glm::vec3( 0.195f,  0.548f,  0.859f );
    c[i++] = glm::vec3( 0.014f,  0.184f,  0.576f );
    c[i++] = glm::vec3( 0.771f,  0.328f,  0.970f );
    c[i++] = glm::vec3( 0.406f,  0.615f,  0.116f );
    c[i++] = glm::vec3( 0.676f,  0.977f,  0.133f );
    c[i++] = glm::vec3( 0.971f,  0.572f,  0.833f );
    c[i++] = glm::vec3( 0.140f,  0.616f,  0.489f );
    c[i++] = glm::vec3( 0.997f,  0.513f,  0.064f );
    c[i++] = glm::vec3( 0.945f,  0.719f,  0.592f );
    c[i++] = glm::vec3( 0.543f,  0.021f,  0.978f );
    c[i++] = glm::vec3( 0.279f,  0.317f,  0.505f );
    c[i++] = glm::vec3( 0.167f,  0.620f,  0.077f );
    c[i++] = glm::vec3( 0.347f,  0.857f,  0.137f );
    c[i++] = glm::vec3( 0.055f,  0.953f,  0.042f );
    c[i++] = glm::vec3( 0.714f,  0.505f,  0.345f );
    c[i++] = glm::vec3( 0.783f,  0.290f,  0.734f );
    c[i++] = glm::vec3( 0.722f,  0.645f,  0.174f );
    c[i++] = glm::vec3( 0.302f,  0.455f,  0.848f );
    c[i++] = glm::vec3( 0.225f,  0.587f,  0.040f );
    c[i++] = glm::vec3( 0.517f,  0.713f,  0.338f );
    c[i++] = glm::vec3( 0.053f,  0.959f,  0.120f );
    c[i++] = glm::vec3( 0.393f,  0.621f,  0.362f );
    c[i++] = glm::vec3( 0.673f,  0.211f,  0.457f );
    c[i++] = glm::vec3( 0.820f,  0.883f,  0.371f );
    c[i++] = glm::vec3( 0.982f,  0.099f,  0.879f );

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), v, GL_STATIC_DRAW);

    glGenBuffers(1, &CBO);
    glBindBuffer(GL_ARRAY_BUFFER, CBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(c), c, GL_STATIC_DRAW);
}
