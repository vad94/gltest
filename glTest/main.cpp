#include <unistd.h>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>

#include "glutils.h"
#include "glengine.h"
#include "trianglerenderer.h"
#include "cube_renderer.h"


int main(int argc, char *argv[])
{
    try
    {
        GLRenderEngine::InitConfig cfg;
        cfg.color_bits = 1;
        cfg.depth_bits = 1;
        cfg.surface_type = GLRenderEngine::InitConfig::SURFACE_WINDOW;
        cfg.width = 640;
        cfg.height = 480;

        unsigned int fps = 1000;

        if (argc > 1)
        {
            if (argc == 4 && (strcmp(argv[1], "-render") == 0))
            {
                fps = std::stoi( argv[2] );
                cfg.surface_type = static_cast<GLRenderEngine::InitConfig::SurfaceType>( std::stoi( argv[3] ));
                printf("Init OpengGL using pbuffer...\n");

                std::cout << "set: " << fps << " " << cfg.surface_type <<  std::endl;
            }
            else
            {
                std::cout << "Invalid arguments: -render [fps] [surface-type]" <<  std::endl;
                return 0;
            }
        }

        boost::shared_ptr<GLRenderEngine> engine(new GLRenderEngine());
        engine->Init(cfg);

        //boost::shared_ptr<TriangleRenderer> triangle_renderer(new TriangleRenderer());
        //engine->InitRenderObject(triangle_renderer);

        boost::shared_ptr<CubeRenderer> cube_renderer(new CubeRenderer());
        engine->InitRenderObject(cube_renderer);

        engine->StartRenderLoop(fps);
    }
    catch (MessageException e)
    {
       printf(e.GetMessage().c_str());
    }

    //event_loop(x_dpy, win, egl_dpy, egl_surf);
    return 0;
}
