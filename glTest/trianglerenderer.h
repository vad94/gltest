#ifndef TRIANGLERENDERER_H
#define TRIANGLERENDERER_H

#include "glengine.h"

static void
make_z_rot_matrix(GLfloat angle, GLfloat *m)
{
   float c = cos(angle * M_PI / 180.0);
   float s = sin(angle * M_PI / 180.0);
   int i;
   for (i = 0; i < 16; i++)
      m[i] = 0.0;
   m[0] = m[5] = m[10] = m[15] = 1.0;

   m[0] = c;
   m[1] = s;
   m[4] = -s;
   m[5] = c;
}

static void
make_scale_matrix(GLfloat xs, GLfloat ys, GLfloat zs, GLfloat *m)
{
   int i;
   for (i = 0; i < 16; i++)
      m[i] = 0.0;
   m[0] = xs;
   m[5] = ys;
   m[10] = zs;
   m[15] = 1.0;
}


static void
mul_matrix(GLfloat *prod, const GLfloat *a, const GLfloat *b)
{
#define A(row,col)  a[(col<<2)+row]
#define B(row,col)  b[(col<<2)+row]
#define P(row,col)  p[(col<<2)+row]
   GLfloat p[16];
   GLint i;
   for (i = 0; i < 4; i++) {
      const GLfloat ai0=A(i,0),  ai1=A(i,1),  ai2=A(i,2),  ai3=A(i,3);
      P(i,0) = ai0 * B(0,0) + ai1 * B(1,0) + ai2 * B(2,0) + ai3 * B(3,0);
      P(i,1) = ai0 * B(0,1) + ai1 * B(1,1) + ai2 * B(2,1) + ai3 * B(3,1);
      P(i,2) = ai0 * B(0,2) + ai1 * B(1,2) + ai2 * B(2,2) + ai3 * B(3,2);
      P(i,3) = ai0 * B(0,3) + ai1 * B(1,3) + ai2 * B(2,3) + ai3 * B(3,3);
   }
   memcpy(prod, p, sizeof(p));
#undef A
#undef B
#undef PROD
}

class TriangleRenderer : public IGLRenderObject
{
private:


GLfloat m_view_rotx;
GLfloat m_view_roty;

GLint m_u_matrix;
GLint m_attr_pos;
GLint m_attr_color;

protected:

    virtual bool Init(boost::weak_ptr<const GLRenderEngine> engine)
    {
        m_view_rotx = 0.0;
        m_view_roty = 0.0;

        m_u_matrix = -1;
        m_attr_pos = 0;
        m_attr_color = 1;

        static const char *fragShaderText =
           "precision mediump float;\n"
           "varying vec4 v_color;\n"
           "void main() {\n"
           "   gl_FragColor = v_color;\n"
           "}\n";
        static const char *vertShaderText =
           "uniform mat4 modelviewProjection;\n"
           "attribute vec4 pos;\n"
           "attribute vec4 color;\n"
           "varying vec4 v_color;\n"
           "void main() {\n"
           "   gl_Position = modelviewProjection * pos;\n"
           "   v_color = color;\n"
           "}\n";

        GLuint fragShader, vertShader, program;
        GLint stat;

        fragShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragShader, 1, (const char **) &fragShaderText, NULL);
        glCompileShader(fragShader);
        glGetShaderiv(fragShader, GL_COMPILE_STATUS, &stat);
        if (!stat)
        {
         GLchar InfoLog[1024];
             glGetShaderInfoLog(fragShader, 1024, NULL, InfoLog);
             printf("Error compiling shader type %d: '%s'\n", GL_FRAGMENT_SHADER, InfoLog);
             //printf(fragShaderText);
         exit(1);
        }

        vertShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertShader, 1, (const char **) &vertShaderText, NULL);
        glCompileShader(vertShader);
        glGetShaderiv(vertShader, GL_COMPILE_STATUS, &stat);
        if (!stat) {
           printf("Error: vertex shader did not compile!\n");
           exit(1);
        }

        program = glCreateProgram();
        glAttachShader(program, fragShader);
        glAttachShader(program, vertShader);
        glLinkProgram(program);

        glGetProgramiv(program, GL_LINK_STATUS, &stat);
        if (!stat) {
           char log[1000];
           GLsizei len;
           glGetProgramInfoLog(program, 1000, &len, log);
           printf("Error: linking:\n%s\n", log);
           exit(1);
        }

        glUseProgram(program);

        if (1) {
           /* test setting attrib locations */
           glBindAttribLocation(program, m_attr_pos, "pos");
           glBindAttribLocation(program, m_attr_color, "color");
           glLinkProgram(program);  /* needed to put attribs into effect */
        }
        else {
           /* test automatic attrib locations */
           m_attr_pos = glGetAttribLocation(program, "pos");
           m_attr_color = glGetAttribLocation(program, "color");
        }

        m_u_matrix = glGetUniformLocation(program, "modelviewProjection");
        printf("Uniform modelviewProjection at %d\n", m_u_matrix);
        printf("Attrib pos at %d\n", m_attr_pos);
        printf("Attrib color at %d\n", m_attr_color);


//fbo test
//        int fbo_width = 512;
//        int fbo_height = 512;
//        GLuint fboHandle, fboTex, depthBuffer;


//        glGenFramebuffers(1, &fboHandle);
//        glGenTextures(1, &fboTex);
//        glGenRenderbuffers(1, &depthBuffer);

//        glBindFramebuffer(GL_FRAMEBUFFER, fboHandle);

//        glBindTexture(GL_TEXTURE_2D, fboTex);
//        glTexImage2D( GL_TEXTURE_2D,
//                     0,
//                     GL_RGBA,
//                     fbo_width, fbo_height,
//                     0,
//                     GL_RGBA,
//                     GL_UNSIGNED_BYTE,
//                     NULL);

    }

    virtual void Draw()
    {
    static const GLfloat verts[3][2] = {
       { -1, -1 },
       {  1, -1 },
       {  0,  1 }
    };
    static const GLfloat colors[3][3] = {
       { 1, 0, 0 },
       { 0, 1, 0 },
       { 0, 0, 1 }
    };
    GLfloat mat[16], rot[16], scale[16];

    /* Set modelview/projection matrix */
    make_z_rot_matrix(m_view_rotx, rot);
    make_scale_matrix(0.5, 0.5, 0.5, scale);
    mul_matrix(mat, rot, scale);
    glUniformMatrix4fv(m_u_matrix, 1, GL_FALSE, mat);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    {
       glVertexAttribPointer(m_attr_pos, 2, GL_FLOAT, GL_FALSE, 0, verts);
       glVertexAttribPointer(m_attr_color, 3, GL_FLOAT, GL_FALSE, 0, colors);
       glEnableVertexAttribArray(m_attr_pos);
       glEnableVertexAttribArray(m_attr_color);

       glDrawArrays(GL_TRIANGLES, 0, 3);

       glDisableVertexAttribArray(m_attr_pos);
       glDisableVertexAttribArray(m_attr_color);
    }
    }

    virtual void Destroy()
    {

    }
};

#endif // TRIANGLERENDERER_H
