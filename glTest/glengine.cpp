/*
@file engine3d.h
@author V. Burenkov (vburenkov@elvees.com)
*/

#include "glengine.h"
#include "glutils.h"

//////////////////////////////////////////////////////////////////////////////////////////
//// GLRenderEngine
//////////////////////////////////////////////////////////////////////////////////////////

GLRenderEngine::GLRenderEngine()
{
}

GLRenderEngine::~GLRenderEngine()
{

}

void GLRenderEngine::Init(const InitConfig &init_config)
{
    EGLint egl_major, egl_minor;

    static const EGLint context_attr[] = {
       EGL_CONTEXT_CLIENT_VERSION, 2,
       EGL_NONE
    };

    switch (init_config.surface_type)
    {
    case InitConfig::SURFACE_WINDOW:
    case InitConfig::SURFACE_HIDED_WINDOW:
        {
        static const EGLint surfce_attr[] = {
           EGL_RED_SIZE, init_config.color_bits,
           EGL_GREEN_SIZE, init_config.color_bits,
           EGL_BLUE_SIZE, init_config.color_bits,
           EGL_DEPTH_SIZE, init_config.depth_bits,
           EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
           EGL_NONE
        };

        x_display = XOpenDisplay(NULL);
        if (!x_display)
           throw MessageException("Error: XOpenDisplay failed");

        egl_display = eglGetDisplay(x_display);
        if (!egl_display)
           throw MessageException("Error: eglGetDisplay failed");

        if (!eglInitialize(egl_display, &egl_major, &egl_minor))
            throw MessageException("Error: eglInitialize failed");

        egl_print_info(egl_display);

        XSetWindowAttributes attr;
        unsigned long mask;
        Window root;
        XVisualInfo *visual_info, visual_template;
        int num_visuals;

        EGLConfig egl_config;
        EGLint egl_num_configs;
        EGLint visual_id;

        root = RootWindow( x_display, DefaultScreen( x_display ) );

        if (!eglChooseConfig( egl_display, surfce_attr, &egl_config, 1, &egl_num_configs)
                || !egl_config || !egl_num_configs)
            throw MessageException("Error: eglChooseConfig failed");

        if (!eglGetConfigAttrib(egl_display, egl_config, EGL_NATIVE_VISUAL_ID, &visual_id))
            throw MessageException("Error: eglGetConfigAttrib failed");

        visual_template.visualid = visual_id;
        visual_info = XGetVisualInfo(x_display, VisualIDMask, &visual_template, &num_visuals);
        if (!visual_info)
            throw MessageException("Error: XGetVisualInfo failed");

        attr.background_pixel = 0;
        attr.border_pixel = 0;
        attr.colormap = XCreateColormap( x_display, root, visual_info->visual, AllocNone);
        attr.event_mask = StructureNotifyMask | ExposureMask | KeyPressMask;
        mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;

        x_window = XCreateWindow( x_display, root, 0, 0, init_config.width, init_config.height,
                     0, visual_info->depth, InputOutput,
                     visual_info->visual, mask, &attr );

        XSizeHints sizehints;
        sizehints.width  = init_config.width;
        sizehints.height = init_config.height;
        sizehints.flags = USSize ;
        XSetNormalHints(x_display, x_window, &sizehints);
        XSetStandardProperties(x_display, x_window, "OpenGL ES", "OpenGL ES",
                               None, (char **)NULL, 0, &sizehints);

        eglBindAPI(EGL_OPENGL_ES_API);

        egl_context = eglCreateContext(egl_display, egl_config, EGL_NO_CONTEXT, context_attr );
        if (!egl_context)
            throw MessageException("Error: eglCreateContext failed");

        EGLint val;
        eglQueryContext(egl_display, egl_context, EGL_CONTEXT_CLIENT_VERSION, &val);
        if (val != 2)
            //throw MessageException("Error: eglQueryContext failed");
            printf("warning: eglQueryContext = %d\n", val);

        egl_surface = eglCreateWindowSurface(egl_display, egl_config, x_window, NULL);
        if (!egl_surface)
            throw MessageException("Error: eglCreateWindowSurface failed");

//        /* sanity checks */
//        {
//           EGLint val;
//           eglQuerySurface(egl_display, *surfRet, EGL_WIDTH, &val);
//           assert(val == width);
//           eglQuerySurface(egl_display, *surfRet, EGL_HEIGHT, &val);
//           assert(val == height);
//           assert(eglGetConfigAttrib(egl_display, config, EGL_SURFACE_TYPE, &val));
//           assert(val & EGL_WINDOW_BIT);
//        }



        if (init_config.surface_type == InitConfig::SURFACE_WINDOW)
            XMapWindow(x_display, x_window);

        if (!eglMakeCurrent(egl_display, egl_surface, egl_surface, egl_context))
            throw MessageException("Error: eglMakeCurrent failed");

        XFree(visual_info);
        }
        break;

    case InitConfig::SURFACE_PBUFFER:
        {

        static const EGLint surface_attr[] = {
                //EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                //EGL_SURFACE_TYPE, EGL_PBUFFER_BIT,
                EGL_RED_SIZE, init_config.color_bits,
                EGL_GREEN_SIZE, init_config.color_bits,
                EGL_BLUE_SIZE, init_config.color_bits,
                EGL_DEPTH_SIZE, init_config.depth_bits,
                EGL_NONE
        };

        static const EGLint pbuffer_attr[] = {
              EGL_WIDTH, 9,//init_config.width,
              EGL_HEIGHT, 9,//init_config.height,
              EGL_NONE,
        };


        egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        if (!egl_display)
            throw MessageException("Error: eglGetDisplay failed");

        if (!eglInitialize(egl_display, &egl_major, &egl_minor))
            throw MessageException("Error: eglInitialize failed");

        egl_print_info(egl_display);

        EGLint numConfigs;
        EGLConfig eglCfg;
        if (!eglChooseConfig(egl_display, surface_attr, &eglCfg, 1, &numConfigs))
            throw MessageException("Error: eglChooseConfig failed");

        egl_context = eglCreateContext(egl_display, eglCfg, EGL_NO_CONTEXT, context_attr);
        if (!egl_context)
            throw MessageException("Error: eglCreateContext failed");

        egl_surface = eglCreatePbufferSurface(egl_display, eglCfg, pbuffer_attr);
        if (!egl_surface)
            throw MessageException("Error: eglCreatePbufferSurface failed");


        eglBindAPI(EGL_OPENGL_API);


        if (!eglMakeCurrent(egl_display, egl_surface, egl_surface, egl_context))
            throw MessageException("Error: eglMakeCurrent failed");
        }
        break;
    }

    gl_print_info();

#if 1 /* test code */
   if (!eglGetProcAddress("glMapBufferOES"))
        throw MessageException("Error: eglGetProcAddress failed");

   glViewport(0, 0, (GLint) init_config.width, (GLint) init_config.height);
   glClearColor(0.4, 0.4, 0.4, 0.0);
#endif

}

void GLRenderEngine::Draw()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    if (m_render_object)
        m_render_object->Draw();

    eglSwapBuffers(egl_display, egl_surface);
}

void GLRenderEngine::Destroy()
{
    if (m_render_object)
        m_render_object->Destroy();

    eglDestroyContext(egl_display, egl_context);
    eglDestroySurface(egl_display, egl_surface);
    eglTerminate(egl_display);

    if (x_display && x_window)
    {
        XDestroyWindow(x_display, x_window);
        XCloseDisplay(x_display);
    }
}

void GLRenderEngine::StartRenderLoop(unsigned int fps)
{
    unsigned long frame_time = 1000/fps;

    Atom wmDeleteMessage = XInternAtom(x_display, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(x_display, x_window, &wmDeleteMessage, 1);

    XEvent event;

    while (true)
    {
        while (XPending(x_display) > 0)
        {
            XNextEvent(x_display, &event);

            switch (event.type)
            {
            case ConfigureNotify:
                glViewport(0, 0, event.xconfigure.width, event.xconfigure.height);
                break;
            case ClientMessage:
                if (event.xclient.data.l[0] == wmDeleteMessage) return;
                break;
            case KeyPress:
                {
                    int code = XLookupKeysym(&event.xkey, 0);
                    if (code == XK_Right)
                        XUnmapWindow(x_display, x_window);
                }
                break;
            }
        }

        static unsigned int frames = 0;
        static unsigned long t = GetTickCount();
        unsigned long p = GetTickCount() - t;
        if (p > 1000)
        {

            t = GetTickCount();
            std::cout << "FPS: " << std::dec << frames/(p/1000)  << std::endl;
            frames = 0;
        }

        unsigned long render_beg = GetTickCount();
        Draw();
        frames++;

        unsigned long render_time = GetTickCount() - render_beg;
        if (render_time < frame_time)
        {
            usleep(1000 * frame_time-render_time);
        }
    }
}


bool GLRenderEngine::InitRenderObject(boost::shared_ptr<IGLRenderObject> render_object)
{
    render_object->Init(shared_from_this());
    m_render_object = render_object;
}

//////////////////////////////////////////////////////////////////////////////////////////
//// IGLRenderObject
//////////////////////////////////////////////////////////////////////////////////////////
IGLRenderObject::IGLRenderObject()
{

}

IGLRenderObject::~IGLRenderObject()
{

}

void IGLRenderObject::Destroy()
{

}
