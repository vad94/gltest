#ifndef CUBE_RENDERER_H
#define CUBE_RENDERER_H

#include <string>
#include <iostream>

using namespace std;

#include "glengine.h"

class CubeRenderer : public IGLRenderObject
{
private:
    GLuint VBO;
    GLuint CBO;
    GLuint gWorldLocation;
    bool m_inited;
    GLubyte *buffer;

    void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
    bool ReadFile(const char* pFileName, string& outFile);
    void CompileShaders();
    void CreateVertexBuffer();

protected:
    virtual bool Init(boost::weak_ptr<const GLRenderEngine> engine);
    virtual void Draw();
    virtual void Destroy();
};

#endif // CUBE_RENDERER_H
