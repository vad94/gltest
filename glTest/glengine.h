/*
@file engine3d.h
@author V. Burenkov (vburenkov@elvees.com)
*/

#ifndef GLENGINE_H
#define GLENGINE_H

#pragma once

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <GLES2/gl2.h>
#include <EGL/egl.h>

class IGLRenderObject;

class GLRenderEngine:
        public boost::noncopyable,
        public boost::enable_shared_from_this<GLRenderEngine>
{
public:
    struct InitConfig
    {
        enum SurfaceType { SURFACE_HIDED_WINDOW = 0, SURFACE_WINDOW = 1, SURFACE_PBUFFER = 2 } surface_type;
        unsigned int width;
        unsigned int height;
        unsigned int color_bits;
        unsigned int depth_bits;
    };

    GLRenderEngine();
    ~GLRenderEngine();

    void Init(const InitConfig &config);
    void Draw();
    void Destroy();

    bool InitRenderObject(boost::shared_ptr<IGLRenderObject> render_object);

    void StartRenderLoop(unsigned int fps);


private:
    Display *x_display;
    Window x_window;

    EGLSurface egl_surface;
    EGLContext egl_context;
    EGLDisplay egl_display;

    boost::shared_ptr<IGLRenderObject> m_render_object;
};

class IGLRenderObject:
        public boost::noncopyable
{
friend class GLRenderEngine;

public:
    IGLRenderObject();
    virtual ~IGLRenderObject();

protected:
    virtual bool Init(boost::weak_ptr<const GLRenderEngine> engine) = 0;
    virtual void Draw() = 0;
    virtual void Destroy();
};

#endif // GLENGINE_H
